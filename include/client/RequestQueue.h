#ifndef FR_MUSICIAN_IPC_PROTOCOL_CLIENT_REQUEST_QUEUE_H
#define FR_MUSICIAN_IPC_PROTOCOL_CLIENT_REQUEST_QUEUE_H


#include <string>
#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>


#include <server/Typedef.h>
#include "Request.pb.h"


namespace fr::musician::ipc_protocol::client {


class RequestQueue {
  public:
    explicit RequestQueue(std::string queueName);

    void send(const Request &request);
    std::string queueName() const;

  private:
    std::shared_mutex _mutex;
    std::array<char, MESSAGE_SIZE> _data;
    boost::interprocess::message_queue _queue;
    const std::string _queueName;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

#ifndef FR_MUSICIAN_IPC_PROTOCOL_IPC_CLIENT_H
#define FR_MUSICIAN_IPC_PROTOCOL_IPC_CLIENT_H


#include <string>
#include <future>
#include <atomic>


#include "ResponseQueue.h"
#include "RequestQueue.h"
#include "PromiseTable.h"
#include "ResponseHandler.h"


namespace fr::musician::ipc_protocol::client {


class IPCClient {
  public:
    explicit IPCClient(std::string queueName);
    ~IPCClient();

    unsigned int clientId() const;
    std::string responseQueueName() const;
    std::string requestQueueName() const;
    bool isRegistered() const;
    void registerClient();
    std::future<Response> send(Request &request);


  private:
    std::thread _handlerThread;
    std::atomic<bool> _registered = false;
    unsigned int _nextMessageId;
    const unsigned int _clientId;
    ResponseHandler _responseHandler;
    RequestQueue _requestQueue;
    PromiseTable _promiseTable;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

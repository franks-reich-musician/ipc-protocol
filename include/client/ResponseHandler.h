#ifndef FR_MUSICIAN_IPC_PROTOCOL_RESPONSE_HANDLER_H
#define FR_MUSICIAN_IPC_PROTOCOL_RESPONSE_HANDLER_H


#include <string>
#include <atomic>


#include "ResponseQueue.h"
#include "PromiseTable.h"


namespace fr::musician::ipc_protocol::client {


class ResponseHandler {
  public:
    ResponseHandler(unsigned int clientId, PromiseTable &promiseTable);

    void operator()();
    void stop();
    std::string queueName() const;


  private:
    std::atomic<bool> _running = true;
    ResponseQueue _queue;
    PromiseTable &_promiseTable;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

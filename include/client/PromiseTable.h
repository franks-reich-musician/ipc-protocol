#ifndef FR_MUSICIAN_IPC_PROTOCOL_PROMISE_TABLE_H
#define FR_MUSICIAN_IPC_PROTOCOL_PROMISE_TABLE_H


#include <future>
#include <shared_mutex>
#include <map>


#include "server/Typedef.h"
#include "Response.pb.h"


namespace fr::musician::ipc_protocol::client {


using response_promise = std::promise<Response>;
using response_future = std::future<Response>;
using promise_map = std::map<unsigned int, response_promise>;


class PromiseTable {
  public:
     response_future addPromise(unsigned int messageId);
     void fulfillPromise(unsigned int messageId, const Response &response);


  private:
    std::shared_mutex _mutex;
    promise_map _promises;
};


}


#endif

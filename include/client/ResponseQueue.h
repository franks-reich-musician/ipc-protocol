#ifndef FR_MUSICIAN_IPC_PROTOCOL_RESPONSE_QUEUE_H
#define FR_MUSICIAN_IPC_PROTOCOL_RESPONSE_QUEUE_H


#include <string>
#include <optional>
#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>


#include "server/Typedef.h"
#include "Response.pb.h"


namespace fr::musician::ipc_protocol::client {


class ResponseQueue {
  public:
    explicit ResponseQueue(std::string queueName);
    ~ResponseQueue();

    std::optional<Response> receive();
    std::string queueName() const;

  private:
    std::shared_mutex _mutex;
    Response _response;
    std::array<char, MESSAGE_SIZE> _data;
    boost::interprocess::message_queue _queue;
    const std::string _queueName;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

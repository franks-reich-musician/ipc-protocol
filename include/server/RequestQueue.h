#ifndef FR_MUSICIAN_IPC_PROTOCOL_SERVER_REQUEST_QUEUE_H
#define FR_MUSICIAN_IPC_PROTOCOL_SERVER_REQUEST_QUEUE_H


#include <string>
#include <array>
#include <optional>


#include <boost/interprocess/ipc/message_queue.hpp>


#include "Typedef.h"
#include "Request.pb.h"


namespace fr::musician::ipc_protocol::server {


class RequestQueue {
  public:
    explicit RequestQueue(const std::string& queueName);
    ~RequestQueue();

    std::optional<Request> receiveRequest();


  private:
    std::shared_mutex _mutex;
    boost::interprocess::message_queue _requestQueue;
    const std::string _queueName;
    std::array<char, MESSAGE_SIZE> _buffer;
    Request _request;
};


} // of namespace fr::musician::ipc_protocol


#endif

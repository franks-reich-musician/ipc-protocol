#ifndef FR_MUSICIAN_IPC_PROTOCOL_IPC_SERVER_H
#define FR_MUSICIAN_IPC_PROTOCOL_IPC_SERVER_H


#include <string>
#include <memory>
#include <thread>


#include "RequestHandler.h"
#include "RequestQueue.h"
#include "ClientRegistry.h"
#include "FunctionRegistry.h"


namespace fr::musician::ipc_protocol::server {


class IPCServer {
  public:
    explicit IPCServer(std::string queueName);
    ~IPCServer();

    void start();
    
    void addRequestHandler(
      Request::DataCase requestType, request_handler_function functor);

    FunctionRegistry &registry();


  private:
    RequestQueue _requestQueue;
    RequestHandler _requestHandler;
    ClientRegistry _clientRegistry;
    std::thread _requestThread;
    FunctionRegistry _functionRegistry;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

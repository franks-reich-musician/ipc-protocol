#ifndef FR_MUSICIAN_IPC_PROTOCOL_CLIENT_H
#define FR_MUSICIAN_IPC_PROTOCOL_CLIENT_H


#include <map>
#include <shared_mutex>


#include <boost/interprocess/ipc/message_queue.hpp>


#include "Typedef.h"
#include "Response.pb.h"


namespace fr::musician::ipc_protocol::server {


class Client {
  public:
    Client(unsigned int clientId, const std::string &queueName);

    unsigned int clientId() const;
    std::string queueName() const;

    void send(const Response &response);

  private:
    const unsigned int _clientId;
    const std::string _queueName;
    std::shared_mutex _mutex;
    boost::interprocess::message_queue _queue;
    std::array<char, MESSAGE_SIZE> _data{};
};


using client_ptr = std::unique_ptr<Client>;
using client_map = std::map<unsigned int, client_ptr>;


} // of namespace fr::musician::ipc_protocol


#endif

#ifndef FR_MUSICIAN_IPC_PROTOCOL_TYPEDEF_H
#define FR_MUSICIAN_IPC_PROTOCOL_TYPEDEF_H


#include <map>
#include <shared_mutex>


#include <boost/interprocess/ipc/message_queue.hpp>


namespace fr::musician::ipc_protocol {


const boost::interprocess::message_queue::size_type MESSAGE_SIZE = 100;
const boost::interprocess::message_queue::size_type QUEUE_SIZE = 10;


using write_lock = std::unique_lock<std::shared_mutex>;
using read_lock = std::shared_lock<std::shared_mutex>;


} // of namespace fr::musician::ipc_protocol


#endif

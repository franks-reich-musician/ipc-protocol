#ifndef FR_MUSICIAN_IPC_PROTOCOL_FUNCTION_REGISTRY_H
#define FR_MUSICIAN_IPC_PROTOCOL_FUNCTION_REGISTRY_H


#include <map>
#include <functional>


#include "Request.pb.h"
#include "Response.pb.h"
#include "ClientRegistry.h"


namespace fr::musician::ipc_protocol::server {


using request_handler_function =
  std::function<void (const Request &, Response &)>;
using function_map = std::map<
  Request::DataCase,
  request_handler_function>;


class FunctionRegistry {
  public:
    void addFunction(
      const Request::DataCase requestType,
      request_handler_function requestHandlerFunction);

    const request_handler_function &operator()(
      const Request::DataCase requestType) const;

    const request_handler_function &function(
      const Request::DataCase requestType) const;


  private:
    function_map _handlerFunctions;
};


} // end of namespace fr::sequencer::ipc_protocol


#endif

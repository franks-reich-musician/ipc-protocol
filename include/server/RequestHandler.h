#ifndef FR_MUSICIAN_IPC_PROTOCOL_REQUEST_HANDLER_H
#define FR_MUSICIAN_IPC_PROTOCOL_REQUEST_HANDLER_H


#include <atomic>
#include <string>


#include "RequestQueue.h"
#include "ClientRegistry.h"
#include "FunctionRegistry.h"


namespace fr::musician::ipc_protocol::server {


class RequestHandler {
  public:
    RequestHandler(
      RequestQueue &requestQueue,
      ClientRegistry &clientRegistry,
      FunctionRegistry &functionRegistry);

    void operator()();
    void stop();


  private:
    std::atomic<bool> _running = true;
    RequestQueue &_requestQueue;
    ClientRegistry &_clientRegistry;
    FunctionRegistry &_functionRegistry;

    void _handleRegistrationRequest(const Request &request);
};


} // end of namespace fr::musician::ipc_protocol


#endif

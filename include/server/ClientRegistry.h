#ifndef FR_MUSICIAN_IPC_PROTOCOL_CLIENT_REGISTRY_H
#define FR_MUSICIAN_IPC_PROTOCOL_CLIENT_REGISTRY_H


#include <map>
#include <shared_mutex>


#include <boost/interprocess/ipc/message_queue.hpp>


#include "Response.pb.h"
#include "Typedef.h"
#include "Client.h"


namespace fr::musician::ipc_protocol::server {


class ClientRegistry {
  public:
    void addClient(const unsigned int clientId, const std::string queueName);

    void send(unsigned int clientId, const Response &response);


  private:
    std::shared_mutex _mutex;
    client_map _clients;
};


} // of namespace fr::musician::ipc_protocol


#endif

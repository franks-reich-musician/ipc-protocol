#include "server/FunctionRegistry.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


void FunctionRegistry::addFunction(
  const Request::DataCase requestType,
  request_handler_function requestHandlerFunction) {

  _handlerFunctions.insert(
    function_map::value_type(requestType, requestHandlerFunction));
}


const request_handler_function &FunctionRegistry::function(
  const Request::DataCase requestType) const
{
  return _handlerFunctions.at(requestType);
}


const request_handler_function &FunctionRegistry::operator()(
  const Request::DataCase requestType) const
{
  return function(requestType);
}

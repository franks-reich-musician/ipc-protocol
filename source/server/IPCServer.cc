#include "server/IPCServer.h"


#include <memory>
#include <utility>
#include <functional>


#include <boost/log/trivial.hpp>
#include <boost/log/sources/severity_logger.hpp>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


IPCServer::IPCServer(string queueName):
  _requestQueue(queueName),
  _requestHandler(_requestQueue, _clientRegistry, _functionRegistry)
  {}


IPCServer::~IPCServer() {
  _requestHandler.stop();
  _requestThread.join();
}


void IPCServer::addRequestHandler(
  Request::DataCase requestType, request_handler_function functor) {

  _functionRegistry.addFunction(requestType, move(functor));
}


void IPCServer::start() {
  _requestThread = thread(ref(_requestHandler));
}


FunctionRegistry &IPCServer::registry() {
  return _functionRegistry;
}

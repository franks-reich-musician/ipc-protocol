#include <boost/log/trivial.hpp>
#include "server/RequestHandler.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


RequestHandler::RequestHandler(
  RequestQueue &requestQueue,
  ClientRegistry &clientRegistry,
  FunctionRegistry &functionRegistry):
  _requestQueue(requestQueue),
  _clientRegistry(clientRegistry),
  _functionRegistry(functionRegistry)
  {}


void RequestHandler::stop() {
  _running = false;
}


void RequestHandler::operator()() {
  while (_running) {
    const auto message = _requestQueue.receiveRequest();
    if (message.has_value()) {
      const Request request = message.value();
      switch (request.data_case()) {
        case Request::DataCase::kRegistration:
          _handleRegistrationRequest(request);
          break;
        case Request::DataCase::DATA_NOT_SET:
          break;
        default:
          BOOST_LOG_TRIVIAL(info)
            << "Received request of type " << request.data_case()
            << " from client id " << request.senderid();
          const auto requestHandler = _functionRegistry.function(request.data_case());
          Response response;
          requestHandler(request, response);
          response.set_messageid(request.messageid());
          _clientRegistry.send(request.senderid(), response);
          break;
      }
    }
  }
}


void RequestHandler::_handleRegistrationRequest(const Request &request) {
  const auto clientId = request.senderid();
  const auto queueName = request.registration().queuename();
  _clientRegistry.addClient(clientId, queueName);
  RegistrationResponse registrationResponse;
  Response response;
  response.set_allocated_registration(&registrationResponse);
  _clientRegistry.send(clientId, response);
  response.release_registration();
}

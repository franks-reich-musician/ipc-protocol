#include "server/RequestQueue.h"


#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/log/trivial.hpp>


using namespace boost::interprocess;
using namespace boost::posix_time;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;


RequestQueue::RequestQueue(const std::string& queueName):
  _requestQueue(create_only, queueName.c_str(), QUEUE_SIZE, MESSAGE_SIZE),
  _queueName(queueName)
  {}


RequestQueue::~RequestQueue() {
  message_queue::remove(_queueName.c_str());
}


std::optional<Request> RequestQueue::receiveRequest() {
  write_lock lock(_mutex);
  message_queue::size_type bytesReceived(0);
  unsigned int priority(0);
  const auto time = second_clock::universal_time() + seconds(2);
  BOOST_LOG_TRIVIAL(info) << "Waiting for message until "
                          << time;
  const auto success = _requestQueue.timed_receive(
    &_buffer, _buffer.size(), bytesReceived, priority, time);
  if (success) {
    _request.ParseFromArray(&_buffer, bytesReceived);
    return _request;
  } else {
    return {};
  }
}

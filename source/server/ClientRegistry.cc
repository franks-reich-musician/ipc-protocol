#include "server/ClientRegistry.h"


#include <memory>
#include <utility>
#include <stdexcept>


#include <boost/log/trivial.hpp>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace boost::interprocess;
using namespace std;


void ClientRegistry::addClient(
  const unsigned int clientId, const std::string queueName) {

  BOOST_LOG_TRIVIAL(info)
    << "Added client with client id " << clientId;

  write_lock lock(_mutex);
  client_ptr client(new Client(clientId, queueName));
  _clients.insert(client_map::value_type(clientId, move(client)));
}


void ClientRegistry::send(unsigned int clientId, const Response &response) {
  read_lock lock(_mutex);
  const auto client = _clients.find(clientId);
  if (client != _clients.end()) {
    client->second->send(response);
  } else {
    throw range_error("Client does not exist.");
  }
}

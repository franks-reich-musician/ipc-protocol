#include "server/Client.h"


#include <memory>
#include <utility>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace boost::interprocess;
using namespace std;


Client::Client(const unsigned int clientId, const std::string &queueName):
  _clientId(clientId),
  _queueName(queueName),
  _queue(open_only, queueName.c_str())
{}


unsigned int Client::clientId() const {
  return _clientId;
}


std::string Client::queueName() const {
  return _queueName;
}


void Client::send(const Response &response) {
  write_lock lock(_mutex);
  response.SerializeToArray(&_data, _data.size());
  _queue.send(&_data, response.ByteSizeLong(), 0);
}

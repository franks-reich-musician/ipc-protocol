#include "client/PromiseTable.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;


response_future PromiseTable::addPromise(unsigned int messageId) {
  write_lock lock(_mutex);
  _promises.insert(promise_map::value_type(messageId, response_promise()));
  return _promises.at(messageId).get_future();
}


void PromiseTable::fulfillPromise(
  unsigned int messageId,
  const Response &response) {

  write_lock lock(_mutex);
  _promises.at(messageId).set_value(response);
}

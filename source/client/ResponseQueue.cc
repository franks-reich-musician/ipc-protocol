#include "client/ResponseQueue.h"


#include <boost/date_time/posix_time/posix_time.hpp>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::interprocess;
using namespace boost::posix_time;
using namespace std;


ResponseQueue::ResponseQueue(std::string queueName):
  _queueName(queueName),
  _queue(create_only, queueName.c_str(), QUEUE_SIZE, MESSAGE_SIZE)
  {}


ResponseQueue::~ResponseQueue() {
  message_queue::remove(_queueName.c_str());
}


std::optional<Response> ResponseQueue::receive() {
  write_lock lock(_mutex);
  message_queue::size_type bytesReceived(0);
  unsigned int priority(0);
  const auto time = second_clock::universal_time() + seconds(2);
  const auto success = _queue.timed_receive(
    &_data, _data.size(), bytesReceived, priority, time);
  if (success) {
    _response.ParseFromArray(&_data, bytesReceived);
    return _response;
  } else {
    return {};
  }
}


std::string ResponseQueue::queueName() const {
  return _queueName;
}

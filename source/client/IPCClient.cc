#include "client/IPCClient.h"


#include <sstream>
#include <random>
#include <exception>


#include <boost/log/trivial.hpp>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace std;


unsigned int _generateClientId() {
  uniform_int_distribution<unsigned int>
    distribution(1, numeric_limits<int>::max());
  random_device randomDevice;
  return distribution(randomDevice);
}


IPCClient::IPCClient(string queueName):
  _clientId(_generateClientId()),
  _requestQueue(queueName),
  _nextMessageId(0),
  _responseHandler(_clientId, _promiseTable),
  _handlerThread(ref(_responseHandler))
  {}


IPCClient::~IPCClient() {
  BOOST_LOG_TRIVIAL(info) << "Shutting down client";
  _responseHandler.stop();
  _handlerThread.join();
}


unsigned int IPCClient::clientId() const {
  return _clientId;
}


std::string IPCClient::responseQueueName() const {
  return _responseHandler.queueName();
}


std::future<Response> IPCClient::send(Request &request) {
  if (request.has_registration() || _registered) {
    const auto messageId = _nextMessageId++;
    request.set_senderid(_clientId);
    request.set_messageid(messageId);
    _requestQueue.send(request);
    return _promiseTable.addPromise(messageId);
  } else {
    throw domain_error("Client is not registered.");
  }
}


std::string IPCClient::requestQueueName() const {
  return _requestQueue.queueName();
}


void IPCClient::registerClient() {
  BOOST_LOG_TRIVIAL(info) << "Register client protocol started";
  Request request;
  RegistrationRequest registrationRequest;
  registrationRequest.set_queuename(responseQueueName());
  request.set_allocated_registration(&registrationRequest);
  auto response = send(request);
  request.release_registration();
  response.wait();
  _registered = true;
}


bool IPCClient::isRegistered() const {
  return _registered;
}

add_library(IPCClient
  ../../include/server/Typedef.h
  ../../include/client/PromiseTable.h
  PromiseTable.cc
  ../../include/client/IPCClient.h
  IPCClient.cc
  ../../include/client/RequestQueue.h
  RequestQueue.cc
  ../../include/client/ResponseQueue.h
  ResponseQueue.cc
  ../../include/client/ResponseHandler.h
  ResponseHandler.cc)
add_library(fr::musician::IPCClient ALIAS IPCClient)

target_link_libraries(IPCClient
  PUBLIC
    Boost::log
    Boost::system
    Boost::thread
    Boost::date_time
    fr::musician::Protocol)

target_include_directories(IPCClient
  PUBLIC
    ../../include)

target_compile_features(IPCClient
  PUBLIC
    cxx_std_17)

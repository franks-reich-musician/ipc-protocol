#include "client/ResponseHandler.h"


#include <boost/log/trivial.hpp>


using namespace fr::musician::ipc_protocol::client;
using namespace std;


string _generateResponseQueueName(unsigned int clientId) {
  stringstream stringStream;
  stringStream << "fr_musician_request_queue_client_" << clientId;
  return stringStream.str();
}


ResponseHandler::ResponseHandler(unsigned int clientId, PromiseTable &promiseTable):
  _queue(_generateResponseQueueName(clientId)),
  _promiseTable(promiseTable)
  {}


void ResponseHandler::stop() {
  BOOST_LOG_TRIVIAL(info) << "Stopping response handler";
  _running = false;
}


void ResponseHandler::operator()() {
  BOOST_LOG_TRIVIAL(info) << "Starting client response handler";
  this_thread::sleep_for(1s);
  while (_running) {
    BOOST_LOG_TRIVIAL(info) << "Receiving response message, "
                            << "running: " << _running;
    const auto optionalResponse = _queue.receive();
    if (optionalResponse.has_value()) {
      const auto response = optionalResponse.value();
      BOOST_LOG_TRIVIAL(info) << "Received response message with request type "
                              << response.data_case();
      _promiseTable.fulfillPromise(response.messageid(), response);
    }
  }
  BOOST_LOG_TRIVIAL(info) << "Completed client response handler";
}


std::string ResponseHandler::queueName() const {
  return _queue.queueName();
}

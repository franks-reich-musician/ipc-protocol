#include "client/RequestQueue.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::interprocess;


RequestQueue::RequestQueue(std::string queueName):
  _queue(open_only, queueName.c_str()),
  _queueName(queueName)
  {}


void RequestQueue::send(const Request &request) {
  write_lock lock(_mutex);
  request.SerializeToArray(&_data, _data.size());
  _queue.send(&_data, request.ByteSizeLong(), 0);
}


std::string RequestQueue::queueName() const {
  return _queueName;
}

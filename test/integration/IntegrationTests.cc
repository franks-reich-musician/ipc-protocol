#include <boost/test/unit_test.hpp>


#include <array>


#include <client/IPCClient.h>
#include <server/IPCServer.h>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::data_definition;
using namespace boost::interprocess;
using namespace boost::unit_test;
using namespace std;


BOOST_AUTO_TEST_SUITE(IntegrationTests)


class IntegrationTestsFixture {
  public:
    ~IntegrationTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    }
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete queues if they still exist")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  integration_test,
  IntegrationTestsFixture,
  *description(
    "The client should be able to connect to the server. After connecting,"
    "the client should be able to send messages to the server and the server"
    "should respond according to the registered handler function.")) {

  server::IPCServer server("test_queue");
  client::IPCClient client("test_queue");

  unsigned int clientId = 0;
  unsigned int eventType = 0;
  unsigned int patternId = 0;
  unsigned int messageId = 0;

  Response response;
  GetPatternResponse getPatternResponse;
  server.addRequestHandler(
    Request::DataCase::kGetPattern,
    [&clientId, &eventType, &patternId,
     &messageId, &response, &getPatternResponse](
      const Request &request, Response &response) {

      clientId = request.senderid();
      eventType = request.data_case();
      patternId = request.getpattern().patternid();
      messageId = request.messageid();

      response.set_messageid(messageId);
      auto pattern = response.mutable_getpattern()->mutable_pattern();
      pattern->set_name("Test pattern");
      pattern->set_id(42);
    });

  server.start();
  client.registerClient();
  BOOST_REQUIRE_EQUAL(client.isRegistered(), true);
  Request request;
  GetPatternRequest getPatternRequest;
  getPatternRequest.set_patternid(55);
  request.set_allocated_getpattern(&getPatternRequest);
  auto responseFuture = client.send(request);
  auto actual = responseFuture.get();
  request.release_getpattern();
  response.release_getpattern();
  getPatternResponse.release_pattern();
}


BOOST_AUTO_TEST_SUITE_END()

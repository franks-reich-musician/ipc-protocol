#include "Utils.h"


#include <array>


#include "Request.pb.h"
#include "server/Typedef.h"


using namespace fr::test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace std;


void fr::test::sendRegistrationRequest(unsigned int clientId, boost::interprocess::message_queue &queue) {
  RegistrationRequest registrationRequest;
  registrationRequest.set_queuename("test_response_queue");
  Request request;
  request.set_senderid(3);
  request.set_allocated_registration(&registrationRequest);
  array<char, MESSAGE_SIZE> data{};
  request.SerializeToArray(&data, data.size());
  queue.send(&data, request.ByteSizeLong(), 0);
  request.release_registration();
}

#include <boost/test/unit_test.hpp>


#include <array>


#include <server/RequestQueue.h>


using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


BOOST_AUTO_TEST_SUITE(RequestQueueTests)


class RequestQueueTestsFixture {
  public:
    RequestQueueTestsFixture():
      requestQueue("test_queue")
      {};

    ~RequestQueueTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    }

    RequestQueue requestQueue;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete message queue if still exists")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  creating_request_message_queue,
  RequestQueueTestsFixture,
  *description(
    "The server should create the request message queue on start up."
    "The queue should have the correct length and message size.")) {

  const message_queue queue(open_only, "test_queue");
  const auto messageSize = queue.get_max_msg_size();
  const auto queueSize = queue.get_max_msg();
  BOOST_REQUIRE_EQUAL(messageSize, MESSAGE_SIZE);
  BOOST_REQUIRE_EQUAL(queueSize, QUEUE_SIZE);
}


BOOST_FIXTURE_TEST_CASE(
  recieving_request_message,
  RequestQueueTestsFixture,
  *description("The request queue should allow to read request messages.")) {

  auto testMessage = Request();
  testMessage.set_senderid(3);
  array<char, MESSAGE_SIZE> data{};
  testMessage.SerializeToArray(&data, data.size());
  message_queue queue(open_only, "test_queue");
  queue.send(&data, testMessage.ByteSizeLong(), 0);
  auto const actual = requestQueue.receiveRequest();
  BOOST_REQUIRE_EQUAL(actual.value().senderid(), testMessage.senderid());
}


BOOST_AUTO_TEST_SUITE_END()

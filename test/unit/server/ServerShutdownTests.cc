#include <boost/test/unit_test.hpp>


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>
#include <google/protobuf/stubs/common.h>


#include <server/IPCServer.h>
#include <server/RequestQueue.h>
#include "Request.pb.h"


using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


BOOST_AUTO_TEST_SUITE(ServerShutdownTests)


class ServerShutdownTestsFixture {
  public:
    ServerShutdownTestsFixture():
      server("test_queue")
      {}

    ~ServerShutdownTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    }

    IPCServer server;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete message queue if still exists")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  server_should_shutdown_correctly,
  ServerShutdownTestsFixture,
  *description("The server should end the request handler when destructed.")) {

  server.start();
}


BOOST_AUTO_TEST_SUITE_END()

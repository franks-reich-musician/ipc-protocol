#include <boost/test/unit_test.hpp>


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>
#include <google/protobuf/stubs/common.h>


#include <server/IPCServer.h>
#include <server/Client.h>
#include <Request.pb.h>


#include "Utils.h"


using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


BOOST_AUTO_TEST_SUITE(GeneralRequestHandlingTests)


class GeneralRequestHandlingTestsFixture {
  public:
    GeneralRequestHandlingTestsFixture():
      server("test_queue"),
      responseQueue(create_only, "test_response_queue", 1, MESSAGE_SIZE)
      {}

    ~GeneralRequestHandlingTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
      message_queue::remove("test_response_queue");
    }

    IPCServer server;
    message_queue responseQueue;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete message queue if still exists")) {

  message_queue::remove("test_queue");
  message_queue::remove("test_response_queue");
}


BOOST_FIXTURE_TEST_CASE(
  general_request_handler,
  GeneralRequestHandlingTestsFixture,
  *description(
    "The server should answer general messages by calling the registered"
    "handler function.")) {

  unsigned int clientId(0);
  unsigned int eventType(0);
  unsigned int patternId(0);


  server.addRequestHandler(
    Request::DataCase::kGetPattern,
    [&clientId, &eventType, &patternId](
      const Request &request, Response &response) {

      clientId = request.senderid();
      eventType = request.data_case();
      patternId = request.getpattern().patternid();

      response.set_messageid(request.messageid());
      response.mutable_getpattern()->mutable_pattern()->set_id(patternId);
    });

  server.start();

  message_queue queue(open_only, "test_queue");
  fr::test::sendRegistrationRequest(3, queue);

  array<char, MESSAGE_SIZE> data{};
  unsigned int bytesReceived(0);
  unsigned int priority(0);
  responseQueue.receive(&data, data.size(), bytesReceived, priority);

  Request request;
  request.set_senderid(3);
  request.set_messageid(5);
  GetPatternRequest getPatternRequest;
  getPatternRequest.set_patternid(5);
  request.set_allocated_getpattern(&getPatternRequest);
  request.SerializeToArray(&data, data.size());
  queue.send(&data, request.ByteSizeLong(), 0);
  request.release_getpattern();

  responseQueue.receive(&data, data.size(), bytesReceived, priority);
  Response response;
  response.ParseFromArray(&data, bytesReceived);
  BOOST_REQUIRE_EQUAL(response.messageid(), 5);
  BOOST_REQUIRE_EQUAL(response.getpattern().pattern().id(), 5);

  BOOST_REQUIRE_EQUAL(clientId, 3);
  BOOST_REQUIRE_EQUAL(eventType, 14);
  BOOST_REQUIRE_EQUAL(patternId, 5);
}


BOOST_AUTO_TEST_SUITE_END()

#ifndef FR_MUSICIAN_IPC_SERVER_TESTS_UTILS_H
#define FR_MUSICIAN_IPC_SERVER_TESTS_UTILS_H


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>


namespace fr::test {


void sendRegistrationRequest(
  unsigned int clientId, boost::interprocess::message_queue &queue);


} // end of namespace fr::test

#endif


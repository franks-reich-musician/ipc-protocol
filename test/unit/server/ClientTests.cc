#include <boost/test/unit_test.hpp>


#include <array>


#include "server/Client.h"
#include "server/Typedef.h"


using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientTests)


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete message queue if still exists")) {

  message_queue::remove("test_queue");
}


BOOST_AUTO_TEST_CASE(
  client_message_queue,
  *description(
    "The client class should open the message queue with the given name and"
    "be able to send request messages to the message queue.")) {

  message_queue queue(create_only, "test_queue", 1, MESSAGE_SIZE);
  Client client(3, "test_queue");
  Response response;
  RegistrationResponse registration;
  response.set_allocated_registration(&registration);
  client.send(response);
  array<char, MESSAGE_SIZE> data{};
  message_queue::size_type bytesReceived(0);
  unsigned int priority(0);
  const auto success = queue.try_receive(
    &data, data.size(), bytesReceived, priority);
  if (success) {
    Response actual;
    actual.ParseFromArray(&data, bytesReceived);
    BOOST_REQUIRE_EQUAL(bytesReceived, response.ByteSizeLong());
    BOOST_ASSERT(actual.has_registration());
  } else {
    BOOST_FAIL("Message was not put in queue!");
  }
  response.release_registration();
  google::protobuf::ShutdownProtobufLibrary();
}


BOOST_AUTO_TEST_SUITE_END()

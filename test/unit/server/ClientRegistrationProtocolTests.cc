#include <boost/test/unit_test.hpp>


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>
#include <google/protobuf/stubs/common.h>


#include <server/IPCServer.h>
#include <server/RequestQueue.h>
#include <Request.pb.h>


#include "Utils.h"


using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::server;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientRegistrationProtocolTests)


class ClientRegistrationProtocolTestsFixture {
  public:
    ClientRegistrationProtocolTestsFixture():
      server("test_queue"),
      responseQueue(create_only, "test_response_queue", 1, MESSAGE_SIZE)
      {}

    ~ClientRegistrationProtocolTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
      message_queue::remove("test_response_queue");
    }

    IPCServer server;
    message_queue responseQueue;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete message queue if still exists")) {

  message_queue::remove("test_queue");
  message_queue::remove("test_response_queue");
}


BOOST_FIXTURE_TEST_CASE(
  client_registration_protocol,
  ClientRegistrationProtocolTestsFixture,
  *description("Register a client at the IPC Server.")) {

  server.start();
  message_queue queue(open_only, "test_queue");
  fr::test::sendRegistrationRequest(3, queue);
  this_thread::sleep_for(1s);
  array<char, MESSAGE_SIZE> responseData{};
  unsigned int bytesReceived(0);
  unsigned int priority(0);
  const bool success = responseQueue.try_receive(
    &responseData, responseData.size(), bytesReceived, priority);
  if (success) {
    Response response;
    response.ParseFromArray(&responseData, bytesReceived);
    BOOST_ASSERT(response.has_registration());
  } else {
    BOOST_FAIL("Did not receive response message");
  }
}


BOOST_AUTO_TEST_SUITE_END()

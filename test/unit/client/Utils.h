#ifndef FR_MUSICIAN_IPC_PROTOCOL_IPC_CLIENT_TEST_UTILS_H
#define FR_MUSICIAN_IPC_PROTOCOL_IPC_CLIENT_TEST_UTILS_H


#include "client/IPCClient.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::interprocess;
using namespace std;


void completeRegistration(
  message_queue &requestQueue, Request &request, IPCClient &client);


#endif

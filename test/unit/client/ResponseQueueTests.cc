#include <boost/test/unit_test.hpp>


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>


#include <server/Typedef.h>
#include <client/ResponseQueue.h>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientResponseQueueTests)


class ResponseQueueTestsFixture {
  public:
    ResponseQueueTestsFixture():
      responseQueue("test_queue")
      {}

    ~ResponseQueueTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    }

    ResponseQueue responseQueue;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete queues if they still exist")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  receive_messages,
  ResponseQueueTestsFixture,
  *description(
    "The response queue should create a message queue. It should also allow to"
    "receive messages from the queue.")) {

  const auto failed = responseQueue.receive();
  BOOST_REQUIRE_EQUAL(failed.has_value(), false);

  message_queue queue(open_only, "test_queue");
  Response response;
  RegistrationResponse registrationResponse;
  response.set_allocated_registration(&registrationResponse);
  array<char, MESSAGE_SIZE> data{};
  response.SerializeToArray(&data, data.size());
  queue.send(&data, response.ByteSizeLong(), 0);
  response.release_registration();

  const auto actual = responseQueue.receive();
  if (actual.has_value()) {
    BOOST_REQUIRE_EQUAL(actual.value().has_registration(), true);
  } else {
    BOOST_FAIL("Message was not put into queue.");
  }
}


BOOST_AUTO_TEST_SUITE_END()

#include <boost/test/unit_test.hpp>


#include <array>


#include <boost/interprocess/ipc/message_queue.hpp>


#include <server/Typedef.h>
#include <client/RequestQueue.h>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientRequestQueueTests)


class RequestQueueTestsFixture {
  public:
    RequestQueueTestsFixture():
      queue(create_only, "test_queue", 1, MESSAGE_SIZE),
      requestQueue("test_queue")
      {}

    ~RequestQueueTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    }

    message_queue queue;
    RequestQueue requestQueue;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete queues if they still exist")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  write_request_to_queue,
  RequestQueueTestsFixture,
  *description(
    "The request queue should connect to the queue with the name queueName and"
    "should allow to write requests to the queue.")) {

  Request request;
  request.set_senderid(34);
  requestQueue.send(request);

  array<char, MESSAGE_SIZE> data{};
  unsigned int bytesReceived(0);
  unsigned int priority(0);
  const bool success = queue.try_receive(
    &data, data.size(), bytesReceived, priority);

  if (success) {
    Request actual;
    actual.ParseFromArray(&data, bytesReceived);
    BOOST_REQUIRE_EQUAL(bytesReceived, request.ByteSizeLong());
    BOOST_REQUIRE_EQUAL(priority, 0);
    BOOST_REQUIRE_EQUAL(actual.senderid(), 34);
  } else {
    BOOST_FAIL("Message was not put into queue.");
  }
}


BOOST_AUTO_TEST_SUITE_END()

#include <boost/test/unit_test.hpp>


#include <client/PromiseTable.h>


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::unit_test;


BOOST_AUTO_TEST_SUITE(PromiseTableTests)


class PromiseTableTestsFixture {
  public:
    ~PromiseTableTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
    };

    PromiseTable promiseTable;
};


BOOST_FIXTURE_TEST_CASE(
  create_and_fulfill_promise,
  PromiseTableTestsFixture,
  *description(
    "The promise table should be able to create promises. When creating a"
    "promise it should return the corresponding future. It should also allow"
    "to fulfill the promise and therefore completing the future")) {

  auto future = promiseTable.addPromise(4);
  Response response;
  response.set_messageid(4);
  promiseTable.fulfillPromise(4, response);
  const auto status = future.wait_for(std::chrono::seconds(0));
  BOOST_REQUIRE_EQUAL(
    static_cast<int>(status),
    static_cast<int>(std::future_status::ready));
  const auto actual = future.get();
  BOOST_REQUIRE_EQUAL(actual.messageid(), response.messageid());
}


BOOST_AUTO_TEST_SUITE_END()

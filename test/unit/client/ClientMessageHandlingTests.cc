#include <boost/test/unit_test.hpp>


#include <thread>


#include "client/IPCClient.h"


#include "Utils.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientMessageHandlingTest)


class ClientMessageHandlingTestsFixture {
  public:
    ClientMessageHandlingTestsFixture():
      requestQueue(create_only, "test_queue", 1, MESSAGE_SIZE),
      client("test_queue")
      {}

    ~ClientMessageHandlingTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
      message_queue::remove("test_queue");
    }

    message_queue requestQueue;
    IPCClient client;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete queues if they still exist")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  request_handling,
  ClientMessageHandlingTestsFixture,
  *description(
    "The ipc client should send requests and return a future for the response."
    "The future should be completed once the response message is received.")) {

  auto mockServer = thread([this]() {
    Request request;
    completeRegistration(requestQueue, request, client);
  });

  client.registerClient();
  mockServer.join();

  Response response;
  Request request;
  auto responseServer = thread([this, &response, &request]() {
    array<char, MESSAGE_SIZE> data{};
    unsigned int bytesReceived(0);
    unsigned int priority(0);
    requestQueue.receive(&data, data.size(), bytesReceived, priority);
    request.ParseFromArray(&data, bytesReceived);

    response.set_messageid(request.messageid());
    response.SerializeToArray(&data, response.ByteSizeLong());
    message_queue queue(open_only, client.responseQueueName().c_str());
    queue.send(&data, response.ByteSizeLong(), 0);
  });

  auto futureResponse = client.send(request);
  responseServer.join();
  BOOST_REQUIRE_EQUAL(request.messageid(), 1);
  auto actualResponse = futureResponse.get();
  BOOST_REQUIRE_EQUAL(actualResponse.messageid(), response.messageid());
}


BOOST_AUTO_TEST_SUITE_END()

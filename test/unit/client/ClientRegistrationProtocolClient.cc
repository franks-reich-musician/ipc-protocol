#include <boost/test/unit_test.hpp>


#include <thread>


#include <boost/log/trivial.hpp>


#include "client/IPCClient.h"


#include "Utils.h"


using namespace fr::musician::ipc_protocol;
using namespace fr::musician::ipc_protocol::client;
using namespace boost::unit_test;
using namespace boost::interprocess;
using namespace std;


BOOST_AUTO_TEST_SUITE(ClientRegistrationProtocolClientTests)


class ClientRegistrationProtocolClientTestsFixture {
  public:
    ClientRegistrationProtocolClientTestsFixture():
      requestQueue(create_only, "test_queue", 1, MESSAGE_SIZE),
      client("test_queue")
      {}

    ~ClientRegistrationProtocolClientTestsFixture() {
      google::protobuf::ShutdownProtobufLibrary();
      message_queue::remove("test_queue");
    }

    message_queue requestQueue;
    IPCClient client;
};


BOOST_AUTO_TEST_CASE(
  delete_queue,
  *description(
    "Delete queues if they still exist")) {

  message_queue::remove("test_queue");
}


BOOST_FIXTURE_TEST_CASE(
  client_registration,
  ClientRegistrationProtocolClientTestsFixture,
  *description(
    "The ipc client should execute the client registration protocol when the"
    "registration is started. It should create the response queue and send a"
    "client registration request to the server via the request queue and"
    "then wait for the servers response.")) {

  Request request;
  auto mockServer = thread([this, &request]() {
    completeRegistration(requestQueue, request, client);
  });
  client.registerClient();
  mockServer.join();
  BOOST_REQUIRE_EQUAL(client.clientId(), request.senderid());
  BOOST_REQUIRE_EQUAL(client.isRegistered(), true);
}


bool isClientNotRegisteredException(domain_error const &error) {
  string expected = "Client is not registered.";
  return expected.compare(error.what()) == 0;
}


BOOST_FIXTURE_TEST_CASE(
  preventing_other_requests_when_not_registered,
  ClientRegistrationProtocolClientTestsFixture,
  *description(
    "The client should not allow sending requests other than the registration"
    "request before the registration protocol is completed.")) {

  Request request;
  BOOST_REQUIRE_EXCEPTION(
    client.send(request),
    domain_error,
    isClientNotRegisteredException);
}


BOOST_AUTO_TEST_SUITE_END()

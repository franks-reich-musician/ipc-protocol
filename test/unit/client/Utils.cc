#include "Utils.h"


void completeRegistration(
  message_queue &requestQueue, Request &request, IPCClient &client) {

  array<char, MESSAGE_SIZE> data{};
  unsigned int bytesReceived(0);
  unsigned int priority(0);
  requestQueue.receive(&data, data.size(), bytesReceived, priority);
  request.ParseFromArray(&data, bytesReceived);

  Response response;
  response.set_messageid(request.messageid());
  RegistrationResponse registrationResponse;
  response.set_allocated_registration(&registrationResponse);
  response.SerializeToArray(&data, response.ByteSizeLong());

  message_queue queue(open_only, client.responseQueueName().c_str());
  queue.send(&data, response.ByteSizeLong(), 0);
  response.release_registration();
}

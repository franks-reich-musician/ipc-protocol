# TODO List

## Implement integration test

 - Implement an integration test that tests client registration and get pattern.
 
## Implement queue receiver

 - Template on message type, queue size, message size and wait time
 - Implement receive from queue
 
## Implement queue sender

 - Template on message type, queue size and message size
 - Implement send to queue
